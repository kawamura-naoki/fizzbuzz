package fizzBuzz;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import fizzBuzz.FizzBuzz;

/**
 *
 * @author kawamura.naoki
 *
 */
public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	 @AfterClass
	 public static void tearDownAfterClass() throws Exception {
		 System.out.println("tearDownAfterClass");
	 }

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("setUp");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown");
	}

	@Test
	/**
	 * {link fizzBuzz.FizzBuzz#FizzBuzzTest(java.lang.String)
	 * の為のテストメソッド(3で割り切れる数)
	 */
	public void testIsSum_01() {
		System.out.println("testIsSum_01");
		assertEquals("Fizz",FizzBuzz.FizzBuzzTest(3));
	}

	/**
	 * {link fizzBuzz.FizzBuzz#FizzBuzzTest(java.lang.String)
	 * の為のテストメソッド(5で割り切れる数)
	 */
	@Test
	public void testIsSum_02() {
		System.out.println("testIsSum_02");
		assertEquals("Buzz",FizzBuzz.FizzBuzzTest(5));
	}

	/**
	 * {link fizzBuzz.FizzBuzz#FizzBuzzTest(java.lang.String)
	 * の為のテストメソッド(3と5で割り切れる数)
	 */
	@Test
	public void testIsSum_03() {
		System.out.println("testIsSum_03");
		assertEquals("FizzBuzz",FizzBuzz.FizzBuzzTest(15));
	}

	/**
	 * {link fizzBuzz.FizzBuzz#FizzBuzzTest(java.lang.String)
	 * の為のテストメソッド割り切れない数)
	 */
	@Test
	public void testIsSum_04() {
		System.out.println("testIsSum_04");
		assertEquals("1",FizzBuzz.FizzBuzzTest(1));
	}
}
