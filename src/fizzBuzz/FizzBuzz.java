package fizzBuzz;

/**
 * FizzBuzzクラス
 */
public class FizzBuzz {

	/**
	 * 入力した数値を返す
	 * @param i
	 *       数値
	 * @return sum
	 */
	static public  String FizzBuzzTest(int i ){
		String sum = Integer.toString(i);
		if( i % 3 == 0){
			sum = "Fizz";
		}
		if( i % 5 == 0){
			sum = "Buzz";
		}
		if( i % 3 == 0 && i % 5 == 0){
			sum = "FizzBuzz";
		}
		return sum;
	}
}

